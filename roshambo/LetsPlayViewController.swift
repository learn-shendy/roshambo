//
//  LetsPlayViewController
//  roshambo
//
//  Created by  Ahmed Shendy on 9/5/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

class LetsPlayViewController: UIViewController {
    
    //MARK: Outlets Properties
    
    @IBOutlet weak var rockButton: UIButton!        // Tag = 0
    @IBOutlet weak var paperButton: UIButton!       // Tag = 1
    @IBOutlet weak var scissorsButton: UIButton!    // Tag = 2
    
    //MARK: Properties
    
    var imageList = ["PaperCoversRock", "RockCrushesScissors", "ScissorsCutPaper"]
    var correspondingMessageList = ["Paper covers Rock", "Rock crushes Scissors", "Scissors cut Paper"]
    
    //MARK: Action Methods

    @IBAction func play(_ sender: UIButton) {
        switch sender {
        case rockButton:
            playRockForm(sender)
        
        case paperButton:
            playPaperForm(sender)
            
        default:
            fatalError("Unsupported button for this game!")
        }
    }
    
    //MARK: Segue Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! ResultViewController
        
        switch segue.identifier! {
        case "paperPlaySegue":
            controller.userChosenForm = 1

        case "scissorsPlaySegue":
            controller.userChosenForm = 2
            
        default:
            fatalError("Unsupported segue \(segue.identifier!)!")
        }
    }
    
    //MARK: Private Methods
    
    // Code-Only
    private func playRockForm(_ sender: UIButton) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController

        controller.userChosenForm = 0
        
        present(controller, animated: true, completion: nil)
    }
    
    // Code-Segue
    private func playPaperForm(_ sender: UIButton) {
        performSegue(withIdentifier: "paperPlaySegue", sender: self)
    }
}

