//
//  ResultViewController.swift
//  roshambo
//
//  Created by  Ahmed Shendy on 9/5/18.
//  Copyright © 2018 Ahmed Shendy. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    //MARK: Properties
    
    let imageList = ["RockCrushesScissors", "PaperCoversRock", "ScissorsCutPaper"]
    let messageList = ["Paper covers Rock", "Rock crushes Scissors", "Scissors cut Paper"]

    var userChosenForm: Int!

    var randomForm: Int! {
        didSet {
            updateImageView()
            updateMessageLabel()
        }
    }
    
    //MARK: Outlet Properties
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    

    //MARK: App Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        randomForm = Int( arc4random() % 3 )
    }
    
    //MARK: Action Methods
    
    @IBAction func playAgain(_ sender: UIButton) {
        userChosenForm = Optional.none
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Privte Methods
    
    private func updateImageView() {
        let imageName = imageList[randomForm]
        
        guard let image = UIImage(named: imageName) else {
            fatalError("Can't find such image in assets with name \(imageName)")
        }
        
        imageView.image = image
    }
    
    private func updateMessageLabel() {
        messageLabel.text = messageList[randomForm] + (randomForm == userChosenForm ? ". You win" : ". You lose.")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
